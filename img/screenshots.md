Test-Driven Development By Example

Kent Beck



Expliquer :

- Exemple utilisé
- Méthodologie

Screenshot I - La peur - Preface - XI

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/1_la_peur.jpg)



2 - Simplifier le réel - Preface - XIII

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/2_simplifier_le_reel.jpg)

3 - Fake it/Obvious - Page 13 (chapitre 2)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/3_fake_it_obvious.jpg)

4 - Triangulation - Page 16 (chapitre 3)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/4_triangulation.jpg)

5 - Le refactoring - Page 29 (chapitre 6)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/5_le_refacto.jpg)

6 - Vitesse variable - Page 42 - Chapitre 9

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/6_vitesse_variable.jpg)

7 - Poser la question à l'ordinateur - Page 46 (chapitre 10)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/7_poser_la_question_a_lordi.jpg)

8 - Etre conservateur - Page 47 (chapitre 10)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/8_etre_conservateur.jpg)

9 - Nombre de lignes - Page 78 (chapitre 16)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/9_nombre_de_lignes.jpg)

10 - Last review - Page 87 (chapitre 17)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie1/10_last_review.jpg)

11 - False Starts - Page 95 (chapitre 18)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie2/11_false_starts.jpg)

12 - Test coupling - Page 98 (chapitre 19)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie2/12_test_coupling.jpg)

12 - Refactoring & undos - Page 102 (chapitre 20)

![](/Projets/github/gsalaun1/relecture-tdd/img/Partie2/13_refactoring_undos.jpg)
